RED =			\033[31m
GRE =			\033[32m
YEL =			\033[33m
BLU =			\033[34m
PUR =			\033[94m
PIN =			\033[1;35m
PRR =			\033[0;36m
STD =			\033[39m

NAME =			libfts.a

SDIR =			./srcs/

SRCS =			ft_bzero.s			\
				ft_strcat.s 		\
				ft_isalpha.s		\
				ft_isdigit.s		\
				ft_isalnum.s		\
				ft_isascii.s		\
				ft_isprint.s		\
				ft_toupper.s		\
				ft_tolower.s		\
				ft_puts.s			\
				ft_strlen.s			\
				ft_memset.s			\
				ft_memcpy.s			\
				ft_strdup.s 		\
				ft_strcpy.s			\
				ft_strcmp.s			\
				ft_memdel.s			\
				ft_memalloc.s		\
				ft_atoi.s			\
				ft_cat.s

SRCC =		$(addprefix $(SRCS))

ODIR =		./objs/

OBJS =		$(SRCS:.s=.o)
OBCC =		$(addprefix $(ODIR),$(OBJS))

CFLAG =		-Wall -Wextra -Werror

# TEST #
NAME_TEST =			test_libftASM
SRCS_TEST =			test.c			\
					utils.c
SDIR_TEST =			./tests/srcs/
SRCC_TEST =			$(addprefix $(SDIR_TEST),$(SRCS_TEST))
ODIR_TEST =			./tests/objs/
OBJS_TEST =			$(SRCS_TEST:.c=.o)
OBCC_TEST =			$(addprefix $(ODIR_TEST),$(OBJS_TEST))
IDIR_TEST =			./tests/inc/
INCS_TEST =			test.h
INCC_TEST =			$(addprefix $(IDIR_TEST),$(INCS_TEST))
LDIR_TEST =			./
LIBS_TEST =			-lfts

all: $(NAME)

$(NAME): header $(OBCC)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@ar rc $(NAME) $(OBCC)
	@echo "  ${PIN}Compilation terminee !${STD}"

$(ODIR)%.o: $(SDIR)%.s
	@echo "  ${PUR}+Compilation :${STD} $^"
	@mkdir -p $(ODIR)
	@nasm -f macho64 $< -o $@

header:
	@echo "${PRR}"
	@echo "  ====================="
	@echo "  |   Projet LibASM   |"
	@echo "  ====================="
	@echo "${STD}"

clean: header
	@echo "  ${RED}-Delete all object files${STD}"
	@rm -rf $(ODIR)
	@rm -f $(OBCC)

fclean: clean
	@rm -f $(NAME)
	@echo "  ${RED}-Delete objects and binary${STD}"

re: fclean all

.PHONY: test
test: $(NAME_TEST)

$(NAME_TEST): header $(OBCC_TEST)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@gcc -g $(CFLAG) $(OBCC_TEST) -L $(LDIR_TEST) $(LIBS_TEST) -o $(NAME_TEST)
	@echo "  ${PIN}Compilation terminee !${STD}"

$(ODIR_TEST)%.o: $(SDIR_TEST)%.c
	@echo "  ${PUR}+Compilation :${STD} $^"
	@mkdir -p $(ODIR_TEST)
	@gcc $^ $(FLAG) -c -o $@ -I$(IDIR_TEST)

test_clean:
	@echo "  ${RED}-Delete all object files${STD}"
	@rm -rf $(ODIR_TEST)
	@rm -f $(OBCC_TEST)

test_fclean: test_clean
	@rm -f $(NAME_TEST)
	@echo "  ${RED}-Delete objects and binary${STD}"

test_re: test_fclean test
