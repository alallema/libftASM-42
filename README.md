# libftASM

## Description
Ce projet a pour but de vous faire coder une minilib en ASM

## Functions 

# Part-1

```void	ft_bzero(void *s, size_t n)
   char    *ft_strcat(char *restrict s1, const char *restrict s2)
   int     ft_isalpha(int c)
   int     ft_isdigit(int c)
   int     ft_isalnum(int c)
   int     ft_isascii(int c)
   int     ft_isprint(int c)
   int     ft_tolower(int c)
   int     ft_toupper(int c)
   int     ft_puts(const char *s)
```

# Part-2

```
    size_t   ft_strlen(const char *s)
    void    *ft_memset(void *b, int c, size_t len)
    void    *ft_memcpy(void *restrict dst, const void *restrict src, size_t n)
    char    *ft_strdup(const char *s1)
```

# Part-3

``` 
    void    ft_cat(int fd)
```


# Bonus

```
    char    *ft_strcpy(char * dst, const char * src)
    void	*ft_memalloc(size_t size)
    int	    ft_strcmp(const char *s1, const char *s2)
    void	ft_memdel(void **ap)
```

# Test

Run test
```
   $> make test
   $> ./test_libftASM
```
For details add -v option

## register

![register](./docs/registers.png)
![register](./docs/Screenshot-13.59.25.png)
![register](./docs/N0KnG.png)

## register usage
![register_usage](./docs/register_usage.png)
