#ifndef LIBFTS_H
# define LIBFTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <assert.h>
#include "color.h"

//** functions from libftasm to test **//

void	ft_bzero(void *s, size_t n);
char	*ft_strcat(char *s1, char *s2);
int		ft_isalpha(int i);
int		ft_isdigit(int i);
int		ft_isalnum(int i);
int		ft_isascii(int i);
int		ft_isprint(int i);
int		ft_tolower(int c);
int		ft_toupper(int c);
int		ft_puts(const char *s);
size_t	ft_strlen(char *s);
void	*ft_memset(void *s, int c, size_t n);
void	*ft_memcpy(void *dst, void *src, size_t n);
char	*ft_strdup(char *s);
void	ft_cat(int fd);

void	ft_memdel(void **ap);
char	*ft_strcpy(char * dst, const char * src);
void	*ft_memalloc(size_t size);
int		ft_strcmp(const char *s1, const char *s2);
int		ft_atoi(const char *s);

//** define function for testing **//

void    test_assert_int(int result, void *c, int bool);
char    *ret_put(size_t size, int (*fn)(const char *), char *input);
char	*bz_test(void (*fn)(void *, size_t), char *s);
char	*memset_test(void *(*fn)(void *, int, size_t), char *s, int i);
void	cat_test(char *str);
char    *ret_cat(void (*fn)(int), int file, char *str);
void	strcmp_test(char *s1, char *s2);
void	memdel_test(char *s);
void	memalloc_test(char *s);

#endif
