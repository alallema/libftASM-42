#ifndef TEST_H
# define TEST_H

#include "libfts.h"

//** globale variable **/

int     score;
int		score_1 = 34;
int		score_2 = 18;
int		score_3 = 15;
int		score_4 = 8;
int		score_5 = 8;

#define test_fn_int(fn_name, tested_char)	\
test_assert_int(ft_ ##fn_name(tested_char) == fn_name(tested_char), (void *)tested_char, 0)

#define test_fn_str0(fn_name, tested_str)	\
test_assert_int(strcmp(ft_ ##fn_name(tested_str), fn_name(tested_str)) == 0, (void *)tested_str, 1)

#define test_fn_str1(fn_name, tested_str)	\
test_assert_int(ft_ ##fn_name(tested_str) == fn_name(tested_str), (void *)tested_str, 1)

#define test_fn_str2(fn_name, s)	\
test_assert_int(strcmp(ret_put(strlen(s), &puts, s), ret_put(strlen(s), &puts, s)) == 0, s, 1);

#define test_fn_str3(fn_name, tested_str)	\
test_assert_int(strcmp(bz_test(&ft_ ##fn_name, tested_str), bz_test(&fn_name, tested_str)) == 0, (void *)tested_str, 1)

#define test_fn_str4(fn_name, tested_str)	\
test_assert_int(strcmp(memset_test(&ft_ ##fn_name, tested_str, 0), memset_test(&fn_name, tested_str, 1)) == 0, memset_test(&fn_name, tested_str, 1), 1)

#define test_fn_str5(fn_name, tested_str)	\
memdel_test(tested_str)

#define test_fn_str6(fn_name, tested_str)	\
memalloc_test(tested_str)

#define test_fn_str7(fn_name, tested_str)	\
cat_test(tested_str)

#define test_fn_db_str1(fn_name, s1, s2)		\
test_assert_int((strcmp(ft_ ##fn_name(strdup(s1), s2), fn_name(strdup(s1), s2))) == 0, (void *)s1, 1)

#define test_fn_db_str2(fn_name, s1, s2)		\
test_assert_int((strcmp(ft_ ##fn_name(strdup(s1), s2, strlen(s2)), fn_name(strdup(s1), s2, strlen(s2)))) == 0, (void *)fn_name(strdup(s1), s2, strlen(s2)), 1)

#define test_fn_db_str3(fn_name, s1, s2)	\
strcmp_test(s1, s2)

//** define suite of test **//

#define suite_test_1(fn_name, nb) \
	test_fn_int(fn_name, 'a'); \
	test_fn_int(fn_name, 'a' - 1); \
	test_fn_int(fn_name, 'm'); \
	test_fn_int(fn_name, 'z'); \
	test_fn_int(fn_name, 'z' + 1); \
	test_fn_int(fn_name, 'A'); \
	test_fn_int(fn_name, 'A' - 1); \
	test_fn_int(fn_name, 'M'); \
	test_fn_int(fn_name, 'Z'); \
	test_fn_int(fn_name, 'Z' + 1); \
	test_fn_int(fn_name, ' '); \
	test_fn_int(fn_name, '%'); \
	test_fn_int(fn_name, '\t'); \
	test_fn_int(fn_name, '\n'); \
	test_fn_int(fn_name, '\v'); \
	test_fn_int(fn_name, '\b'); \
	test_fn_int(fn_name, '\n'); \
	test_fn_int(fn_name, '\0'); \
	test_fn_int(fn_name, '0'); \
	test_fn_int(fn_name, '0' - 1); \
	test_fn_int(fn_name, '5'); \
	test_fn_int(fn_name, '9'); \
	test_fn_int(fn_name, '9' + 1); \
	test_fn_int(fn_name, 0); \
	test_fn_int(fn_name, 1); \
	test_fn_int(fn_name, 127); \
	test_fn_int(fn_name, 128); \
	test_fn_int(fn_name, -128); \
	test_fn_int(fn_name, -129); \
	test_fn_int(fn_name, 9999); \
	test_fn_int(fn_name, -42); \
	test_fn_int(fn_name, 31); \
	test_fn_int(fn_name, 32); \
	test_fn_int(fn_name, 33);

#define suite_test_2(fn_name, nb)		\
	test_fn_str ##nb(fn_name, "");		\
	test_fn_str ##nb(fn_name, "\t");	\
	test_fn_str ##nb(fn_name, "\n");	\
	test_fn_str ##nb(fn_name, "\0");	\
	test_fn_str ##nb(fn_name, "1");		\
	test_fn_str ##nb(fn_name, "-1");	\
	test_fn_str ##nb(fn_name, "+1");	\
	test_fn_str ##nb(fn_name, "	+1");	\
	test_fn_str ##nb(fn_name, "12");	\
	test_fn_str ##nb(fn_name, "     123");	\
	test_fn_str ##nb(fn_name, "-123");	\
	test_fn_str ##nb(fn_name, "2147483647");	\
	test_fn_str ##nb(fn_name, "2147483648");	\
	test_fn_str ##nb(fn_name, "-2147483647");	\
	test_fn_str ##nb(fn_name, "-2147483648");	\
	test_fn_str ##nb(fn_name, "lala 		petit 	chat 							");	\
	test_fn_str ##nb(fn_name, "lala		\
	petit\
	chat");	\
	test_fn_str ##nb(fn_name, "0000000 cf fa ed fe 07 00 00 01 03 00 00 00 01 00 00 00\
0000010 02 00 00 00 b0 00 00 00 00 00 00 00 00 00 00 00\
0000020 19 00 00 00 98 00 00 00 00 00 00 00 00 00 00 00\
0000030 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\
0000040 0a 00 00 00 00 00 00 00 d0 00 00 00 00 00 00 00\
0000050 0a 00 00 00 00 00 00 00 07 00 00 00 07 00 00 00\
0000060 01 00 00 00 00 00 00 00 5f 5f 74 65 78 74 00 00\
0000070 00 00 00 00 00 00 00 00 5f 5f 54 45 58 54 00 00\
0000080 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\
0000090 0a 00 00 00 00 00 00 00 d0 00 00 00 00 00 00 00\
00000a0 00 00 00 00 00 00 00 00 00 04 00 80 00 00 00 00\
00000b0 00 00 00 00 00 00 00 00 02 00 00 00 18 00 00 00\
00000c0 e0 00 00 00 01 00 00 00 f0 00 00 00 0b 00 00 00\
00000d0 48 31 c0 48 89 f1 fc f3 aa c3 00 00 00 00 00 00\
00000e0 01 00 00 00 0f 01 00 00 00 00 00 00 00 00 00 00\
00000f0 00 5f 66 74 5f 62 7a 65 72 6f 00               \
00000fb");

#define suite_test_3(fn_name, nb)		\
	test_fn_db_str ##nb(fn_name, "", "");		\
	test_fn_db_str ##nb(fn_name, "\t", "");		\
	test_fn_db_str ##nb(fn_name, "\n", "");		\
	test_fn_db_str ##nb(fn_name, "\0", "\0");	\
	test_fn_db_str ##nb(fn_name, "", "a");		\
	test_fn_db_str ##nb(fn_name, "a", "");		\
	test_fn_db_str ##nb(fn_name, "a", "a");		\
	test_fn_db_str ##nb(fn_name, "", "ab");		\
	test_fn_db_str ##nb(fn_name, "", "abc");	\
	test_fn_db_str ##nb(fn_name, "abd", "abc");	\
	test_fn_db_str ##nb(fn_name, "1", "a");		\
	test_fn_db_str ##nb(fn_name, "12", "a");	\
	test_fn_db_str ##nb(fn_name, "123", "a");	\
	test_fn_db_str ##nb(fn_name, "lala			\
	petit\
	chat", "db_a");\
	test_fn_db_str ##nb(fn_name, "0000000 cf fa ed fe 07 00 00 01 03 00 00 00 01 00 00 00\
0000010 02 00 00 00 b0 00 00 00 00 00 00 00 00 00 00 00\
0000020 19 00 00 00 98 00 00 00 00 00 00 00 00 00 00 00\
0000030 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\
0000040 0a 00 00 00 00 00 00 00 d0 00 00 00 00 00 00 00\
0000050 0a 00 00 00 00 00 00 00 07 00 00 00 07 00 00 00\
0000060 01 00 00 00 00 00 00 00 5f 5f 74 65 78 74 00 00\
0000070 00 00 00 00 00 00 00 00 5f 5f 54 45 58 54 00 00\
0000080 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\
0000090 0a 00 00 00 00 00 00 00 d0 00 00 00 00 00 00 00\
00000a0 00 00 00 00 00 00 00 00 00 04 00 80 00 00 00 00\
00000b0 00 00 00 00 00 00 00 00 02 00 00 00 18 00 00 00\
00000c0 e0 00 00 00 01 00 00 00 f0 00 00 00 0b 00 00 00\
00000d0 48 31 c0 48 89 f1 fc f3 aa c3 00 00 00 00 00 00\
00000e0 01 00 00 00 0f 01 00 00 00 00 00 00 00 00 00 00\
00000f0 00 5f 66 74 5f 62 7a 65 72 6f 00               \
00000fb", "");

#define suite_test_4(fn_name, nb)		\
	test_fn_str ##nb(fn_name, "docs/N0KnG.png");\
	test_fn_str ##nb(fn_name, "ressources/Assembler.pdf");		\
	test_fn_str ##nb(fn_name, "libfts.a");		\
	test_fn_str ##nb(fn_name, "test_libftASM");	\
	test_fn_str ##nb(fn_name, "README.md");		\
	test_fn_str ##nb(fn_name, "tests/empty_file");	\
	test_fn_str ##nb(fn_name, "srcs/ft_atoi.s");\
	test_fn_str ##nb(fn_name, "objs/ft_atoi.o");

#define test_name(fn_name, nb_test, nb)				\
void		 test_ ##fn_name()						\
{													\
	score = 0;										\
	printf(BOLD_PUR"  =====================\n");	\
	printf(BOLD_PUR"  %s :\n", __func__);			\
	printf(BOLD_PUR"  =====================\n\n");	\
	printf(STD);									\
	suite_test_##nb_test(fn_name, nb)				\
	if (score == score_ ##nb_test)					\
	{												\
		printf(BOLD_GRE"  SUCCESS");				\
	}												\
	else											\
	{												\
		printf(BOLD_RED"  ERROR");					\
	}												\
	printf(BOLD_YEL"%*c total test: %d/%d\n\n", 30, ' ', score, score_ ##nb_test);	\
	printf(STD);									\
}

test_name(isalpha, 1, 0);
test_name(isdigit, 1, 0);
test_name(isalnum, 1, 0);
test_name(isascii, 1, 0);
test_name(isprint, 1, 0);
test_name(tolower, 1, 0);
test_name(toupper, 1, 0);
test_name(strlen, 2, 1);
test_name(puts, 2, 2);
test_name(strcat, 3, 1);
test_name(bzero, 2, 3);
test_name(memset, 2, 4);
test_name(memcpy, 3, 2);
test_name(strdup, 2, 0);
test_name(strcmp, 3, 3);
test_name(strcpy, 3, 1);
test_name(memdel, 2, 5);
test_name(memalloc, 2, 6);
test_name(atoi, 2, 1);
test_name(cat, 4, 7);

#endif
