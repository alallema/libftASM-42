#ifndef COLOR_H
# define COLOR_H
//** color **//

#define TRUE		1
#define FALSE		0
#define RED 		"\033[31m"
#define BOLD_RED 	"\033[1;31m"
#define GRE 		"\033[0;32m"
#define BOLD_GRE 	"\033[1;32m"
#define YEL 		"\033[0;33m"
#define BOLD_YEL 	"\033[1;33m"
#define BLU 		"\033[0;34m"
#define BOLD_BLU 	"\033[1;34m"
#define PUR 		"\033[0;94m"
#define BOLD_PUR	"\033[1;35m"
#define TUR 		"\033[0;36m"
#define STD 		"\033[0;39m"

#endif