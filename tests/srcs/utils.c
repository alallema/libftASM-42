#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include "libfts.h"
#include "color.h"

extern	int	verbose;
extern  int score;
int		random_value;

void	cat_test(char *str)
{
	int		fd;
	char	*s;
	char	*command;
	char	*s1;

	fd = open(str, O_RDONLY);
	if (fd == -1)
	{
		printf("Error open file: %s\n", str);
		return ;
	}
	s = ret_cat(&ft_cat, fd, str);
	asprintf(&command, "cat %s", str);
	s1 = ret_cat(&ft_cat, -1, command);
	test_assert_int(strcmp(s, s1) == 0, str, 1);
}

void	memalloc_test(char *s)
{
	char	*s1;

	s1 = ft_memalloc(strlen(s));
	strcpy(s1, s);
	free(s1);
	test_assert_int(1, "", 1);
}

void	memdel_test(char *s)
{
	char	*s1;

	s1 = malloc(128);
	strcpy(s1, s);
	ft_memdel((void *)&s1);
	if (s1 == NULL)
		test_assert_int(1, s1, 1);
	else
		test_assert_int(0, s1, 1);
}

void	strcmp_test(char *s1, char *s2)
{
	test_assert_int((strcmp(s1, s2) == ft_strcmp(s1, s2)), s1, 1);
}

char	*memset_test(void *(*fn)(void *, int, size_t), char *s, int i)
{
	if (i == 0)
	{
		i = 0;
		random_value = rand() % 125;
	}
	char *s1;
	s1 = strdup(s);
	return (fn(s1, random_value, strlen(s1)));
}

char	*bz_test(void (*fn)(void *, size_t), char *s)
{
	char *s1;
	s1 = strdup(s);
	fn(s1, strlen(s1));
	return (s1);
}

void    test_assert_int(int result, void *ptr, int bool)
{
	char *s;
//    printf("RESULT %d\n", result);
    if (result == 1)
    {
        if (verbose == TRUE)
        {
            if (bool == FALSE)
			{
				asprintf(&s, "tested - \"%c\"", (char)ptr);
				printf(GRE"  %-30s%s\n", s, "result : OK");
			}
            else
			{
				asprintf(&s, "tested - \"%s\"", (char *)ptr);
				printf(GRE"  %-30s%s\n", s, "result : OK");
			}
        }
        score++;
    }
    else if (result == 0)
    {
        if (verbose == TRUE)
        {
            if (bool == FALSE)
			{
				asprintf(&s, "tested - \"%c\"", (char)ptr);
				printf(RED"  %-30s%s\n", s, "result : OK");
			}
            else
			{
				asprintf(&s, "tested - \"%s\"", (char *)ptr);
				printf(RED"  %-30s%s\n", s, "result : OK");
			}
        }
    }
}

char    *ret_put(size_t size, int (*fn)(const char *), char *input)
{
    int     fd[2];
    char	buff[size + 1];
	char	*str;

    pipe(fd);
	if (fork() == 0) {
		close(fd[0]);
		dup2(fd[1], 1);
		fn(input);
		close(fd[1]);
		exit(0);
	}
    else {
		bzero(buff, size + 1);
		close(fd[1]);
		read(fd[0], buff, size + 1);
		close(fd[0]);
		str = strdup(buff);
	}
    return str;
}

char    *ret_cat(void (*fn)(int), int file_fd, char *file_name)
{
    int     fd[2];
    char	buff[1024];
	char	*str;

    pipe(fd);
	if (fork() == 0) {
		close(fd[0]);
		dup2(fd[1], 1);
		if (file_fd == -1)
			system(file_name);
		else
			fn(file_fd);
		close(fd[1]);
		exit(0);
	}
    else {
		bzero(buff, 1024);
		close(fd[1]);
		read(fd[0], buff, 1024);
		close(fd[0]);
		str = strdup(buff);
	}
    return str;
}
