#include "libfts.h"
#include "test.h"

int verbose = FALSE;

extern int score;

int main(int ac, char **av)
{
    if (ac == 2 && strcmp(av[1], "-v") == 0)
        verbose = TRUE;
    test_isalpha();
    test_isdigit();
    test_isalnum();
    test_isascii();
    test_isprint();
    test_tolower();
    test_toupper();
    test_strlen();
	test_strcat();
	test_bzero();
	test_memset();
	test_memcpy();
	test_strdup();
	test_strcmp();
	test_memdel();
	test_strcpy();
    test_puts();
	test_memalloc();
	test_atoi();
	test_cat();
    // printf("%s", ret_puts(100, &puts, ""));
    // printf("%s", ret_puts(100, &ft_puts, ""));
    // suite_test_2(strcat)
  return(0);
}
